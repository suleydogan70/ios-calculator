//
//  ViewController.swift
//  calculator_suleyman_guillaume_solenne
//
//  Created by Solenne Daguerre on 03/01/2020.
//  Copyright © 2020 MDS. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var numberOnScreen:Double = 0;
    var previousNumber:Double = 0;
    var performingMath = false;
    var operation = 0;
    
    @IBOutlet weak var label: UILabel!
    
    @IBAction func numbers(_ sender: UIButton)
    {
        if performingMath == true {
            label.text = String(sender.tag-1)
            numberOnScreen = Double(label.text!)!
            performingMath = false
        } else {
            label.text = label.text! + String(sender.tag-1)
            numberOnScreen = Double(label.text!)!
        }
    }
    @IBAction func buttons(_ sender: UIButton)
    {
        if label.text != "" && sender.tag != 11 && sender.tag != 16 && sender.tag != 17
        {
            previousNumber = Double(label.text!)!
            
            if sender.tag == 12 {
                label.text = "/";
            }
            else if sender.tag == 13 {
                label.text = "x";
            }
            else if sender.tag == 14 {
                label.text = "-";
            }
            else if sender.tag == 15 {
                label.text = "+";
            }
            else if sender.tag == 18 {
                label.text = "%";
            }
            operation = sender.tag;
            performingMath = true;
        }
        else if sender.tag == 16
        {
            if operation == 12
            {
                label.text = String(previousNumber / numberOnScreen)
            }
            else if operation == 13
            {
                label.text = String(previousNumber * numberOnScreen)
            }
            else if operation == 14
            {
                label.text = String(previousNumber - numberOnScreen)
            }
            else if operation == 15
            {
                label.text = String(previousNumber + numberOnScreen)
            }
//            else if operation == 18
//            {
//                label.text = String(previousNumber % numberOnScreen)
//            }
        }
        else if sender.tag == 11
        {
            label.text = "";
            previousNumber = 0;
            numberOnScreen = 0;
            operation = 0;
        }
        else if sender.tag == 17 {
            label.text = label.text! + "."
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

